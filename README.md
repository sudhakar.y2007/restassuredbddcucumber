To Test the couple of API services by using the REST-ASSURED Framework with BDD Cucumber style. Built this project;

Dependencies:

1. Java version >= 1.8 version
2. JDK
3. Any IDE tools such as IntelliJ IDEA, Eclipse..etc.
4. Below Libraies;
    i. cucumber-junit
    ii. cucumber-java
    iii.rest-assured
    iv.json-schema-validator
    v.json-path


To execute the test, Follow the below steps;

1. Clone the project from the below repo;
   
2. Open the feature file and Right click on the feature file and select "Run 'Feature:*********' Option and provide the Glue value as "steps.stepDefinitions" in the configuration Tab.

3. Able to see the results in the Console.
