$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("GetPost.feature");
formatter.feature({
  "line": 1,
  "name": "End to End Tests for Social Network API",
  "description": "Description: The purpose of these tests are to cover End to End happy flows and couple of negative for Social Network users.\n\nBelow are the base URL \u003d \"https://jsonplaceholder.typicode.com/\"\nand Couple of End Point URL\u0027s\n1. Make posts: https://jsonplaceholder.typicode.com/posts\n2. Comment on posts: https://jsonplaceholder.typicode.com/comments\n3. List of users: https://jsonplaceholder.typicode.com/users\n\n/*\nImagine you are building a social network. Starting from simple functionality. Users are now\nable to make posts and comment on them. You are working in the backend team that\nexposes the service: https://jsonplaceholder.typicode.com/ which has the following\nendpoints:\n\n1. Make posts: https://jsonplaceholder.typicode.com/posts\n2. Comment on posts: https://jsonplaceholder.typicode.com/comments\n3. List of users: https://jsonplaceholder.typicode.com/users\n*/",
  "id": "end-to-end-tests-for-social-network-api",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 22,
      "value": "#    Given I am an authorized user"
    },
    {
      "line": 23,
      "value": "#  Scenario: Validate the GET call with Valid Response"
    },
    {
      "line": 24,
      "value": "#"
    },
    {
      "line": 25,
      "value": "#  Given I perform GET operation for \"/post\""
    },
    {
      "line": 26,
      "value": "#  And I perform GET for the post number \"\""
    },
    {
      "line": 27,
      "value": "#  Then I should see the author name as \"\""
    },
    {
      "line": 28,
      "value": "#"
    },
    {
      "line": 29,
      "value": "#"
    },
    {
      "line": 30,
      "value": "#  Scenario: Validate the POST call with Valid Response"
    },
    {
      "line": 31,
      "value": "#"
    },
    {
      "line": 32,
      "value": "#    Given I perform POST operation for \"/post\""
    },
    {
      "line": 34,
      "value": "#  Scenario Outline: Validate the POST call with Valid Response"
    },
    {
      "line": 35,
      "value": "#   Given User POST the API request \"\" and"
    },
    {
      "line": 36,
      "value": "#    Given User sets the base API request \"\u003cURL\u003e\""
    },
    {
      "line": 37,
      "value": "#    And User sends the API request to get all the users"
    },
    {
      "line": 38,
      "value": "#    Then User validate the response status is \"\u003cCode\u003e\""
    },
    {
      "line": 39,
      "value": "#    Examples:"
    },
    {
      "line": 40,
      "value": "#      | URL                                        | Code |"
    },
    {
      "line": 41,
      "value": "#      | https://jsonplaceholder.typicode.com/posts | 201  |"
    },
    {
      "line": 42,
      "value": "#      | https://jsonplaceholder.typicode.com/post  | 404  |"
    },
    {
      "line": 43,
      "value": "#"
    },
    {
      "line": 44,
      "value": "#  @user @positive"
    },
    {
      "line": 45,
      "value": "#  Scenario: Enter a valid userId"
    },
    {
      "line": 46,
      "value": "#    When I input a valid userId \"1\""
    },
    {
      "line": 47,
      "value": "#    Then I should have the status code \"200\""
    },
    {
      "line": 48,
      "value": "#    And content type should be in \"JSON\" format"
    },
    {
      "line": 49,
      "value": "#    And the body response content should be matched"
    },
    {
      "line": 50,
      "value": "#      | key        | value                  |"
    },
    {
      "line": 51,
      "value": "#      | id         | 1                      |"
    },
    {
      "line": 52,
      "value": "#      | name       | Leanne Graham          |"
    },
    {
      "line": 53,
      "value": "#      | username   | Bret                   |"
    },
    {
      "line": 54,
      "value": "#      | email      | Sincere@april.biz      |"
    },
    {
      "line": 55,
      "value": "#      | phone      | 1-770-736-8031 x56442  |"
    },
    {
      "line": 56,
      "value": "#      | website    | hildegard.org          |"
    },
    {
      "line": 59,
      "value": "#"
    },
    {
      "line": 60,
      "value": "#"
    },
    {
      "line": 61,
      "value": "#  @CommentOnPost-POSTMethod"
    },
    {
      "line": 62,
      "value": "#  Scenario Outline: Enter an invalid UserId"
    },
    {
      "line": 63,
      "value": "#    Given When User make a post by POST API \"\u003cURL\u003e\" get the response \"\u003cCode\u003e\""
    },
    {
      "line": 64,
      "value": "#    Examples:"
    },
    {
      "line": 65,
      "value": "#      | URL                                           | Code |"
    },
    {
      "line": 66,
      "value": "#      | https://jsonplaceholder.typicode.com/comments | 201  |"
    },
    {
      "line": 67,
      "value": "#      | https://jsonplaceholder.typicode.com/comment  | 404  |"
    },
    {
      "line": 68,
      "value": "#"
    },
    {
      "line": 69,
      "value": "#  @GetListOfUsers-GETMethod"
    },
    {
      "line": 70,
      "value": "#  Scenario Outline: Enter an invalid UserId"
    },
    {
      "line": 71,
      "value": "#    Given When User make a request for specific User \"\u003cpathParameter\u003e\" by using the GET \"\u003cURI\u003e\" with queryParameter \"\u003cqueryParameter\u003e\" and User should get the response code as \"\u003cresponseCode\u003e\""
    },
    {
      "line": 72,
      "value": "#    Examples:"
    },
    {
      "line": 73,
      "value": "#      | URI                                   | pathParameter | queryParameter | responseCode |"
    },
    {
      "line": 74,
      "value": "#      | https://jsonplaceholder.typicode.com/ | users         | 1              | 200          |"
    },
    {
      "line": 75,
      "value": "#      | https://jsonplaceholder.typicode.com/ | users         | 2              | 200          |"
    },
    {
      "line": 76,
      "value": "#      | https://jsonplaceholder.typicode.com/ | user          | 1              | 404          |"
    },
    {
      "line": 77,
      "value": "#      | https://jsonplaceholder.typicode.com/ | users         | 100            | 200          |"
    }
  ],
  "line": 80,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 79,
      "name": "@GetListOfAllUsers-GETMethod"
    }
  ]
});
formatter.step({
  "line": 81,
  "name": "When User request GET API \"\u003cURI\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 82,
  "name": "User expect the response code \"\u003cresponseCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 83,
  "name": "User expect the response statusLineCode \"\u003cstateLineCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 84,
  "name": "User expect the response content type \"\u003ccontentType\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "User validate the response schema as per \"\u003cschemaSample\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 86,
  "name": "",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;",
  "rows": [
    {
      "cells": [
        "URI",
        "queryParameter",
        "responseCode",
        "stateLineCode",
        "contentType",
        "schemaSample"
      ],
      "line": 87,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;1"
    },
    {
      "cells": [
        "https://jsonplaceholder.typicode.com/users/",
        "",
        "200",
        "HTTP/1.1 200 OK",
        "application/json",
        "allUsers.json"
      ],
      "line": 88,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;2"
    },
    {
      "cells": [
        "https://jsonplaceholder.typicode.com/user/",
        "",
        "404",
        "HTTP/1.1 404 Not Found",
        "application/json",
        "empty.json"
      ],
      "line": 89,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 21,
  "name": "User generates token for Authorization",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.scenario({
  "line": 88,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 79,
      "name": "@GetListOfAllUsers-GETMethod"
    }
  ]
});
formatter.step({
  "line": 81,
  "name": "When User request GET API \"https://jsonplaceholder.typicode.com/users/\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 82,
  "name": "User expect the response code \"200\"",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 83,
  "name": "User expect the response statusLineCode \"HTTP/1.1 200 OK\"",
  "matchedColumns": [
    3
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 84,
  "name": "User expect the response content type \"application/json\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "User validate the response schema as per \"allUsers.json\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.background({
  "line": 21,
  "name": "User generates token for Authorization",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.scenario({
  "line": 89,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 79,
      "name": "@GetListOfAllUsers-GETMethod"
    }
  ]
});
formatter.step({
  "line": 81,
  "name": "When User request GET API \"https://jsonplaceholder.typicode.com/user/\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 82,
  "name": "User expect the response code \"404\"",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 83,
  "name": "User expect the response statusLineCode \"HTTP/1.1 404 Not Found\"",
  "matchedColumns": [
    3
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 84,
  "name": "User expect the response content type \"application/json\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "User validate the response schema as per \"empty.json\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenarioOutline({
  "line": 92,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 91,
      "name": "@GetListOfSpecificUser-GETMethod"
    }
  ]
});
formatter.step({
  "line": 93,
  "name": "When User request GET API \"\u003cURI\u003e\" with queryParameter \"\u003cqueryParameter\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 94,
  "name": "User expect the response code \"\u003cresponseCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 95,
  "name": "User expect the response statusLineCode \"\u003cstateLineCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "User expect the response content type \"\u003ccontentType\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 97,
  "name": "User validate the response schema as per \"\u003cschemaSample\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 98,
  "name": "",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;",
  "rows": [
    {
      "cells": [
        "URI",
        "queryParameter",
        "responseCode",
        "stateLineCode",
        "contentType",
        "schemaSample"
      ],
      "line": 99,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;1"
    },
    {
      "cells": [
        "https://jsonplaceholder.typicode.com/users",
        "1",
        "200",
        "HTTP/1.1 200 OK",
        "application/json",
        "user.json"
      ],
      "line": 100,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;2"
    },
    {
      "cells": [
        "https://jsonplaceholder.typicode.com/users",
        "2",
        "200",
        "HTTP/1.1 200 OK",
        "application/json",
        "user.json"
      ],
      "line": 101,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 21,
  "name": "User generates token for Authorization",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.scenario({
  "line": 100,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 91,
      "name": "@GetListOfSpecificUser-GETMethod"
    }
  ]
});
formatter.step({
  "line": 93,
  "name": "When User request GET API \"https://jsonplaceholder.typicode.com/users\" with queryParameter \"1\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 94,
  "name": "User expect the response code \"200\"",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 95,
  "name": "User expect the response statusLineCode \"HTTP/1.1 200 OK\"",
  "matchedColumns": [
    3
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "User expect the response content type \"application/json\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 97,
  "name": "User validate the response schema as per \"user.json\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.background({
  "line": 21,
  "name": "User generates token for Authorization",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.scenario({
  "line": 101,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 91,
      "name": "@GetListOfSpecificUser-GETMethod"
    }
  ]
});
formatter.step({
  "line": 93,
  "name": "When User request GET API \"https://jsonplaceholder.typicode.com/users\" with queryParameter \"2\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 94,
  "name": "User expect the response code \"200\"",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 95,
  "name": "User expect the response statusLineCode \"HTTP/1.1 200 OK\"",
  "matchedColumns": [
    3
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "User expect the response content type \"application/json\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 97,
  "name": "User validate the response schema as per \"user.json\"",
  "matchedColumns": [
    5
  ],
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenarioOutline({
  "line": 105,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 104,
      "name": "@MakeaPost-POSTMethod"
    }
  ]
});
formatter.step({
  "line": 106,
  "name": "When User request POST API \"\u003cURI\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 107,
  "name": "User expect the response code \"\u003cresponseCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 108,
  "name": "User expect the response statusLineCode \"\u003cstateLineCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 109,
  "name": "User expect the response content type \"\u003ccontentType\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 110,
  "name": "User validate the response schema as per \"\u003cschemaSample\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 111,
  "name": "",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;",
  "rows": [
    {
      "cells": [
        "URI",
        "responseCode",
        "stateLineCode",
        "contentType",
        "schemaSample"
      ],
      "line": 112,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;1"
    },
    {
      "cells": [
        "https://jsonplaceholder.typicode.com/posts",
        "201",
        "HTTP/1.1 201 Created",
        "application/json",
        "postSchema.json"
      ],
      "line": 113,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;2"
    },
    {
      "cells": [
        "https://jsonplaceholder.typicode.com/post",
        "404",
        "HTTP/1.1 404 Not Found",
        "application/json",
        "empty.json"
      ],
      "line": 114,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 21,
  "name": "User generates token for Authorization",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.scenario({
  "line": 113,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 104,
      "name": "@MakeaPost-POSTMethod"
    }
  ]
});
formatter.step({
  "line": 106,
  "name": "When User request POST API \"https://jsonplaceholder.typicode.com/posts\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 107,
  "name": "User expect the response code \"201\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 108,
  "name": "User expect the response statusLineCode \"HTTP/1.1 201 Created\"",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 109,
  "name": "User expect the response content type \"application/json\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 110,
  "name": "User validate the response schema as per \"postSchema.json\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.background({
  "line": 21,
  "name": "User generates token for Authorization",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.scenario({
  "line": 114,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 104,
      "name": "@MakeaPost-POSTMethod"
    }
  ]
});
formatter.step({
  "line": 106,
  "name": "When User request POST API \"https://jsonplaceholder.typicode.com/post\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 107,
  "name": "User expect the response code \"404\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 108,
  "name": "User expect the response statusLineCode \"HTTP/1.1 404 Not Found\"",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 109,
  "name": "User expect the response content type \"application/json\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 110,
  "name": "User validate the response schema as per \"empty.json\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenarioOutline({
  "line": 117,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 116,
      "name": "@MakeaComment-POSTMethod"
    }
  ]
});
formatter.step({
  "line": 118,
  "name": "When User request POST API \"\u003cURI\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 119,
  "name": "User expect the response code \"\u003cresponseCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 120,
  "name": "User expect the response statusLineCode \"\u003cstateLineCode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 121,
  "name": "User expect the response content type \"\u003ccontentType\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 122,
  "name": "User validate the response schema as per \"\u003cschemaSample\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 123,
  "name": "",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;",
  "rows": [
    {
      "cells": [
        "URI",
        "responseCode",
        "stateLineCode",
        "contentType",
        "schemaSample"
      ],
      "line": 124,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;1"
    },
    {
      "cells": [
        "https://jsonplaceholder.typicode.com/comments",
        "201",
        "HTTP/1.1 201 Created",
        "application/json",
        "postSchema.json"
      ],
      "line": 125,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;2"
    },
    {
      "cells": [
        "https://jsonplaceholder.typicode.com/comment",
        "404",
        "HTTP/1.1 404 Not Found",
        "application/json",
        "empty.json"
      ],
      "line": 126,
      "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 21,
  "name": "User generates token for Authorization",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.scenario({
  "line": 125,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 116,
      "name": "@MakeaComment-POSTMethod"
    }
  ]
});
formatter.step({
  "line": 118,
  "name": "When User request POST API \"https://jsonplaceholder.typicode.com/comments\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 119,
  "name": "User expect the response code \"201\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 120,
  "name": "User expect the response statusLineCode \"HTTP/1.1 201 Created\"",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 121,
  "name": "User expect the response content type \"application/json\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 122,
  "name": "User validate the response schema as per \"postSchema.json\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.background({
  "line": 21,
  "name": "User generates token for Authorization",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.scenario({
  "line": 126,
  "name": "Enter an invalid UserId",
  "description": "",
  "id": "end-to-end-tests-for-social-network-api;enter-an-invalid-userid;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 116,
      "name": "@MakeaComment-POSTMethod"
    }
  ]
});
formatter.step({
  "line": 118,
  "name": "When User request POST API \"https://jsonplaceholder.typicode.com/comment\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 119,
  "name": "User expect the response code \"404\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 120,
  "name": "User expect the response statusLineCode \"HTTP/1.1 404 Not Found\"",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 121,
  "name": "User expect the response content type \"application/json\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 122,
  "name": "User validate the response schema as per \"empty.json\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});