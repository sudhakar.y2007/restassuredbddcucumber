import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {
                "src/test/java/steps/features"
        },
        glue = {
        "steps.stepDefinitions.GetPostSteps.java",
//        "steps.$01BaseSteps"
},
        plugin = {
                "pretty",
                "html:results/html/cucumber.html"
        },
        monochrome = true
)
public class TestRunner {

}