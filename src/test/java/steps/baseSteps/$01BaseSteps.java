package steps.baseSteps;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.post;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class $01BaseSteps {
    public static Response response;
    private static final String BASIC_URL = "https://jsonplaceholder.typicode.com/";

    public static void SimpleGetPost(String contentType, String url, int responseCode) {
//        given().contentType(ContentType.JSON).
        given().contentType(contentType).
                when().post(String.format(url)).
                then().
                assertThat().statusCode(responseCode);

    }

    public static void PerformPOSTWithBodyParameter(String idValue) {
        HashMap<String, String> postContent = new HashMap<>();
        postContent.put("id", idValue);
        given().contentType(ContentType.JSON).
//                with().body(postContent).
        when().post(String.format("https://jsonplaceholder.typicode.com/posts")).
                then().statusCode(201);

    }

    public static Response getRequest(String endpoint) {
        RestAssured.defaultParser = Parser.JSON;
        return
                given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
                        when().get(endpoint).
                        then().contentType(ContentType.JSON).extract().response();
    }

    public static Response getRequestWithQueryParameter(String endpoint, int queryParameterValue) {

        RestAssured.baseURI = BASIC_URL;
        RequestSpecification request = RestAssured.given();
        response = request.queryParam("id", queryParameterValue)
                .get("/users");
        String jsonString = response.asString();
        return response;

    }

    public static Response postRequest(String endpoint) {

        Map<String, String> post = new HashMap<>();
        post.put("id", "101");
        response = given()
                .contentType("application/json")
                .body(post)
                .when().post(endpoint).then().extract().response();
        return response;
    }

    public static void getRequestSize(String endpoint) {
        response = getRequest(endpoint);
        List<String> jsonResponse = response.jsonPath().getList("$");
        System.out.println(jsonResponse.size());
    }

    public static void getRequestWithURI(String endpoint) {
        response = getRequest(endpoint);
        logMessage("Trigger the Request with the API:", endpoint + "/");
    }

    public static void ValidateResponseCode(int StatusCode) {
        System.out.println(response.statusCode());
        int responseCode = response.statusCode();
        assertEqualTo(StatusCode, responseCode);
    }

    public static void validateResponseTime(double expResponseTime) {
        double responseTime = response.timeIn(TimeUnit.MILLISECONDS);
        logMessage("The time taken to fetch the response is:", responseTime);
        Assert.assertTrue(expResponseTime > responseTime);
    }

    public static void validateResponseContentType(String expContentType) {
        String resContentType = response.contentType();
        logMessage("StatusCode of the above Request is:", resContentType);
        Assert.assertThat(resContentType, containsString(expContentType));
    }

    public static void logMessage(String CopyText, Object string) {
        System.out.println(CopyText + string);
    }

    public static void getRequestResponseStatusLine(String expStatusLineCode) {
        String resStatusLineCode = response.statusLine();
        logMessage("StatusCode of the above Request is:", resStatusLineCode);
        assertEqualTo(expStatusLineCode, resStatusLineCode);
    }

    public static void validateJsonResponseConformsToSchemaIsCorrect(String fileName) {
        response.then().assertThat().body(matchesJsonSchemaInClasspath(fileName));
    }

    public static void assertEqualTo(Object expValue, Object actValue) {
        logMessage(String.format("Expected ContentType is : %s", expValue), "");
        logMessage(String.format("Actual ContentType is: %s", actValue), "");
        Assert.assertEquals(expValue, actValue);
    }

    public static void getRequestResponseKey(String endpoint, String[] Key, String KeyValue) {
        Response response = getRequest(endpoint);

        List<String> jsonResponse = response.jsonPath().getList("$");
        for (int i = 0; i < jsonResponse.size(); i++) {


            String actKeyValue = response.jsonPath().getString(Key[i]);
            if (actKeyValue.equalsIgnoreCase(KeyValue)) {
                System.out.println("Key Name is :" + Key[i]);
                List<Map<String, String>> companies = response.jsonPath().getList(Key[i]);
                System.out.println(companies.get(i).get("name"));
                break;
            }
        }
    }

    public static void getSpecificPartOfResponseBody(String inputKeyName, String Value) {

        ArrayList<String> keyName = response.path(inputKeyName);
        for (String k : keyName) {
            Assert.assertEquals(k, Value);
        }
    }

}
