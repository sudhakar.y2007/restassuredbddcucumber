Feature: End to End Tests for Social Network API
  Description: The purpose of these tests are to cover End to End happy flows and couple of negative for Social Network users.

  Below are the base URL = "https://jsonplaceholder.typicode.com/"
  and Couple of End Point URL's
  1. Make posts: https://jsonplaceholder.typicode.com/posts
  2. Comment on posts: https://jsonplaceholder.typicode.com/comments
  3. List of users: https://jsonplaceholder.typicode.com/users


  Background: User generates token for Authorization
    Given I am an authorized user


  @GetListOfAllUsers-GET-CALL
  Scenario Outline: Enter an invalid UserId
    Given When User request GET API "<URI>"
    Then User expect the response code "<responseCode>"
    Then User expect the response statusLineCode "<stateLineCode>"
    And User expect the response content type "<contentType>"
    And User validate the response schema as per "<schemaSample>"
    And User validate the response time is not more than "<responseTime>"
    Examples:
      | URI                                         | queryParameter | responseCode | stateLineCode          | contentType      | schemaSample  | responseTime |
      | https://jsonplaceholder.typicode.com/users/ |                | 200          | HTTP/1.1 200 OK        | application/json | allUsers.json | 1000         |
      | https://jsonplaceholder.typicode.com/user/  |                | 404          | HTTP/1.1 404 Not Found | application/json | empty.json    | 1000         |

  @GetASpecificUserDetails-GET-CALL
  Scenario Outline: Enter an invalid UserId
    Given When User request GET API "<URI>" with queryParameter "<queryParameter>"
    Then User expect the response code "<responseCode>"
    Then User expect the response statusLineCode "<stateLineCode>"
    And User expect the response content type "<contentType>"
    And User validate the response schema as per "<schemaSample>"
    And User validate the specific key "<Key1>" value as "<Value1>"
    And User validate the specific key "<Key2>" value as "<Value2>"
    And User validate the specific key "<Key3>" value as "<Value3>"
    Examples:
      | URI                                        | queryParameter | responseCode | stateLineCode   | contentType      | schemaSample | Key1 | Value1        | Key2     | Value2    | Key3  | Value3            |
      | https://jsonplaceholder.typicode.com/users | 1              | 200          | HTTP/1.1 200 OK | application/json | user.json    | name | Leanne Graham | username | Bret      | email | Sincere@april.biz |
      | https://jsonplaceholder.typicode.com/users | 2              | 200          | HTTP/1.1 200 OK | application/json | user.json    | name | Ervin Howell  | username | Antonette | email | Shanna@melissa.tv |


  @MakeAPost-POST-CALL
  Scenario Outline: Enter an invalid UserId
    Given When User request POST API "<URI>"
    Then User expect the response code "<responseCode>"
    Then User expect the response statusLineCode "<stateLineCode>"
    And User expect the response content type "<contentType>"
    And User validate the response schema as per "<schemaSample>"
    And User validate the response time is not more than "<responseTime>"
    Examples:
      | URI                                        | responseCode | stateLineCode          | contentType      | schemaSample    | responseTime |
      | https://jsonplaceholder.typicode.com/posts | 201          | HTTP/1.1 201 Created   | application/json | postSchema.json | 1000         |
      | https://jsonplaceholder.typicode.com/post  | 404          | HTTP/1.1 404 Not Found | application/json | empty.json      | 1000         |

  @MakeAComment-POST-CALL
  Scenario Outline: Enter an invalid UserId
    Given When User request POST API "<URI>"
    Then User expect the response code "<responseCode>"
    Then User expect the response statusLineCode "<stateLineCode>"
    And User expect the response content type "<contentType>"
    And User validate the response schema as per "<schemaSample>"
    And User validate the response time is not more than "<responseTime>"
    Examples:
      | URI                                           | responseCode | stateLineCode          | contentType      | schemaSample    | responseTime |
      | https://jsonplaceholder.typicode.com/comments | 201          | HTTP/1.1 201 Created   | application/json | postSchema.json | 1000         |
      | https://jsonplaceholder.typicode.com/comment  | 404          | HTTP/1.1 404 Not Found | application/json | empty.json      | 1000         |

