package steps.stepDefinitions;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import steps.baseSteps.$01BaseSteps;


public class SocialNetworkAPISteps {
    private $01BaseSteps SocialNetworkAPIBase;
    private RequestSpecification request;
    private Response response;
    private static final String BASE_URL = "https://jsonplaceholder.typicode.com/";


    /**
     * provision for the Authorized APIs
     */
    @Given("^I am an authorized user$")
    public void iAmAnAuthorizedUser() {
    }

    /**
     * To trigger the GET Request with endpoint
     *
     * @param endpoint endpoint (including PathParameter only)
     */
    @Given("^When User request GET API \"([^\"]*)\"$")
    public void whenUserRequestGETAPI(String endpoint) {
        SocialNetworkAPIBase.getRequestWithURI(endpoint);
    }

    /**
     * To validate the response Code for the API request trigger
     *
     * @param StatusCode Expected StatusCode for the validation w.r.t response StatusCode
     */
    @Then("^User expect the response code \"([^\"]*)\"$")
    public void userExpectTheResponseCode(int StatusCode) {
        SocialNetworkAPIBase.ValidateResponseCode(StatusCode);
    }

    /**
     * To validate the response Content Type for the API Request trigger
     *
     * @param expContentType Expected Content Type for the validation w.r.t response Content Type
     */
    @And("^User expect the response content type \"([^\"]*)\"$")
    public void userExpectTheResponseContentType(String expContentType) {
        SocialNetworkAPIBase.validateResponseContentType(expContentType);
    }

    /**
     * To Validate the response statusLineCode for the API Request trigger
     *
     * @param statusLineCode Expected StatusLineCode for the validation w.r.t response StatusLineCode
     */

    @Then("^User expect the response statusLineCode \"([^\"]*)\"$")
    public void userExpectTheResponseStatusLineCode(String statusLineCode) {
        SocialNetworkAPIBase.getRequestResponseStatusLine(statusLineCode);
    }

    /**
     * To Validate the schema against the JSON schema file reference in the project directory
     *
     * @param fileName JSON Schema file reference for the validation
     */

    @And("^User validate the response schema as per \"([^\"]*)\"$")
    public void userValidateTheResponseSchemaAsPer(String fileName) {
        SocialNetworkAPIBase.validateJsonResponseConformsToSchemaIsCorrect(fileName);
    }

    /**
     * Trigger the POST request with endpoint plus PathParameter
     *
     * @param uri endpoint (including PathParameter only)
     */
    @Given("^When User request POST API \"([^\"]*)\"$")
    public void whenUserRequestPOSTAPI(String uri) {
        SocialNetworkAPIBase.postRequest(uri);
    }

    /**
     * Trigger the GET request with query parameter
     *
     * @param uri                 endpoint (including PathParameter)
     * @param queryParameterValue queryParameter to test with different query Parameter
     */
    @Given("^When User request GET API \"([^\"]*)\" with queryParameter \"([^\"]*)\"$")
    public void whenUserRequestGETAPIWithQueryParameter(String uri, int queryParameterValue) {
        SocialNetworkAPIBase.getRequestWithQueryParameter(uri, queryParameterValue);
    }

    /**
     * Getting the specific key value from the JSON response
     *
     * @param key   JSON Response KeyName
     * @param value JSON Response KeyValue
     */
    @And("^User validate the specific key \"([^\"]*)\" value as \"([^\"]*)\"$")
    public void userValidateTheSpecificKeyValueAs(String key, String value) {
        SocialNetworkAPIBase.getSpecificPartOfResponseBody(key, value);
    }

    /**
     * Validate the individual API Call response time
     *
     * @param expResponseTime expected response Time in MilliSeconds
     */
    @And("^User validate the response time is not more than \"([^\"]*)\"$")
    public void userValidateTheResponseTimeIsNotMoreThan(double expResponseTime) {
        SocialNetworkAPIBase.validateResponseTime(expResponseTime);

    }
}
